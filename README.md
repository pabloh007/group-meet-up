# group-meet-up

2020-01-30 - Setting up git on your machine.
- first we need to download git scm from the following link https://git-scm.com/downloads
- install and follow directions!
- once installed you should be able to search on your installed programs for GitBash
- once you run it, it will be a command line window or terminal. something like this
```bash
pablo@pablo-HP-ProDesk-600-G2-SFF:~/repos/group-meet-up$
```
-run can try running some commands like

```bash
# you can run this command to list out the contents of the directory
# this lists out the directory contents in last-modified order
pablo@pablo-HP-ProDesk-600-G2-SFF:~/repos/group-meet-up$ ls -lrt
total 28
-rw-r--r-- 1 pablo pablo   60 Jan 30 19:51 README.md
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:52 saul
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:53 maren
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:53 pablo
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:53 fernando
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:53 walter
drwxr-xr-x 2 pablo pablo 4096 Jan 30 19:53 cesar
```
- check that you have `ssh` installed, if you installed gitbash it should be installed by default

```bash
ssh -V
OpenSSH_7.9p1 Ubuntu-10, OpenSSL 1.1.1b  26 Feb 2019
```
 - so in git we have something called repositories this where we store code. The benefit of having repositories is you can version changes you make to your code. so lets say you make a mistake on your code. you can revert back to the way it was in a previous revision. Some of the syntax will take some getting used to but it will become more natural as you use it.
 - to clone the repository, you have to issue the following command
 ```bash
 git clone https://gitlab.com/pabloh007/group-meet-up.git
 ```
 this will create a copy of the folder structure in your machine/computer. This is a copy and you can make an changes you want to them
 - once you make code changes and you make changes to your work and are happy with them. you can push your changes wit the following commands.
 ```bash
 #  you must be inside the folder you cloned to commit/push/pull changes
 cd group-meet-up
 #  the below will add all changes you have made and stage them to be committed.
  git add --all
 # then you need to commit your changes with a message about what you did usually a brief summary will suffice.
 git commit -am "This is my first commit of many"
 # finally push the changes up to the cloud so that other people can also see your work
 git push
 ```
 - make sure you make some changes to the files inside your name folder. Git is smart enough to detect nothing was changed and it wont push anything.
 - finally if got this far congratulate yourself as you just pushed your first code commit of many!
