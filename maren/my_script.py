#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Program to read give an input of a file."""

# Added logging
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    )


f = open("/Users/marenbabcock/practice-development/group-meet-up/maren/test_file.txt", "r")
print(f.read())
