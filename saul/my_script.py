#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Program to read give an input of a file."""
# built-ins
import logging

# PIP Installs


# local Modules

# create and setup logger for the program
LOGGER = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    )

my_file = open("test_file.txt", "r")
LOGGER.debug(my_file.read())
print ("this is the way")
LOGGER.debug("this is the way")
LOGGER.info("show me the way")
